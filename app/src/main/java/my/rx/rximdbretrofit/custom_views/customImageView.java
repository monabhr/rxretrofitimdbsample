package my.rx.rximdbretrofit.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;

public class customImageView extends AppCompatImageView {
    Context mContext;

    public customImageView(Context context) {
        super(context);
        mContext = context;

    }

    public customImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

    }

    public customImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;

    }


    public void loadIamge(String url) {
        Glide.with(mContext)
                .load(url)
                .into(this);


    }
}
