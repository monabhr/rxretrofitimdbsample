package my.rx.rximdbretrofit.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import my.rx.rximdbretrofit.utils.BaseApplication;

public class customEditText extends AppCompatEditText {
    public customEditText(Context context) {
        super(context);
    }

    public customEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public String text()
    {
        return (this.getText() != null ? this.getText().toString() : "");
    }
}
