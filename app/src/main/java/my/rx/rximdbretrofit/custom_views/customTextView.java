package my.rx.rximdbretrofit.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import my.rx.rximdbretrofit.utils.BaseApplication;

public class customTextView extends AppCompatTextView {

    public customTextView(Context context) {
        super(context);
    }

    public customTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
