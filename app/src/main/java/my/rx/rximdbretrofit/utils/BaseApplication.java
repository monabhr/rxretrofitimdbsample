package my.rx.rximdbretrofit.utils;

import android.app.Application;


public class BaseApplication extends Application {

    public static BaseApplication baseApp; //singleton

    //public static Typeface typeFace;


    @Override
    public void onCreate() {
        super.onCreate();

        baseApp = this;
    }
}
