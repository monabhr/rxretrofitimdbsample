package my.rx.rximdbretrofit.RetroIMDB;

import android.os.Bundle;
import android.view.View;

import my.rx.rximdbretrofit.R;
import my.rx.rximdbretrofit.custom_views.customEditText;
import my.rx.rximdbretrofit.custom_views.customImageView;
import my.rx.rximdbretrofit.custom_views.customTextView;
import my.rx.rximdbretrofit.utils.BaseActivity;
import my.rx.rximdbretrofit.utils.utilities;


public class RetroImdbActivity extends BaseActivity implements contract.view {

    contract.Presenter presenter = new Presenter(this);

    customEditText movieName;
    customImageView moviePoster;
    customTextView movieRate,movieDirector,movieYear,movieGenre,movieActors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retro_imdb);

        bind();
    }

    private void bind() {

        movieName = findViewById(R.id.movieName);
        moviePoster = findViewById(R.id.moviePoster);
        movieRate = findViewById(R.id.movieRate);
        movieDirector = findViewById(R.id.movieDirector);
        movieYear = findViewById(R.id.movieYear);
        movieGenre = findViewById(R.id.movieGenre);
        movieActors = findViewById(R.id.movieActors);

        findViewById(R.id.searchMovie).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                presenter.onSearchMovie(movieName.text());
            }
        });


    }


    @Override
    public void onFailed() {
        dialog.hide();
        utilities.toast("Can not connect to server ! ");
    }

    @Override
    public void onSuccess(IMDBModel imdbModel) {
        dialog.hide();
        if (imdbModel != null) {
            moviePoster.loadIamge(imdbModel.getPoster());
            movieRate.setText(imdbModel.getImdbRating());
            movieDirector.setText(imdbModel.getDirector());
            movieYear.setText(imdbModel.getYear());
            movieGenre.setText(imdbModel.getGenre());
            movieActors.setText(imdbModel.getActors());
        }
        else
        {
            utilities.toast("No results found !");
        }
    }
}
