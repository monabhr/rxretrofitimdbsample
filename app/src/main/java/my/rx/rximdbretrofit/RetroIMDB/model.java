package my.rx.rximdbretrofit.RetroIMDB;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class model implements contract.model {

    private contract.Presenter presenter;

    serviceInterface serviceInterface =
            retroGenerator.createService(serviceInterface.class);

    @Override
    public void attachPresenter(contract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void onSearchMovie(String searchText) {

        try {

            serviceInterface.searchMovie(searchText, "70ad462a").enqueue(new Callback<IMDBModel>() {
                @Override
                public void onResponse(Call<IMDBModel> call, Response<IMDBModel> response) {
                    presenter.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<IMDBModel> call, Throwable t) {
                    presenter.onFailed();
                }
            });
        } catch (Exception ex) {


        }

    }
}
