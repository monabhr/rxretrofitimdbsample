package my.rx.rximdbretrofit.RetroIMDB;

public interface contract {

    interface view {
        void onFailed();
        void onSuccess(IMDBModel imdbModel);
    }

    interface Presenter {

        void onSearchMovie(String searchText);
        void onFailed();
        void onSuccess(IMDBModel imdbModel);
    }

    interface model {
        void attachPresenter(Presenter presenter);
        void onSearchMovie(String searchText);

    }

}


