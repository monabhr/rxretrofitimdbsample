package my.rx.rximdbretrofit.RetroIMDB;

public class Presenter implements contract.Presenter {

    contract.view view;
    model model = new model();


    public Presenter(contract.view view) {
        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void onSearchMovie(String searchText) {
        model.onSearchMovie(searchText);
    }

    @Override
    public void onFailed() {
        view.onFailed();
    }

    @Override
    public void onSuccess(IMDBModel imdbModel) {
        view.onSuccess(imdbModel);

    }
}
