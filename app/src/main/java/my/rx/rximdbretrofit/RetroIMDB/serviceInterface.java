package my.rx.rximdbretrofit.RetroIMDB;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface serviceInterface {

    //?t=gladiator&apikey=70ad462a

    @GET("/")
    Call<IMDBModel> searchMovie(
            @Query("t") String searchWord
            ,
            @Query("apiKey") String apiKey
    );
}
