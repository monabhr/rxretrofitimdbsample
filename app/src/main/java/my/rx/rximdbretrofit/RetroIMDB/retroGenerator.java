package my.rx.rximdbretrofit.RetroIMDB;

import java.util.concurrent.TimeUnit;

import my.rx.rximdbretrofit.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class retroGenerator {

    public static <S> S createService(  Class<S> serviceClass ) {

        HttpLoggingInterceptor loggerInterceptorBody = new HttpLoggingInterceptor();
        loggerInterceptorBody.setLevel(HttpLoggingInterceptor.Level.BODY);

        HttpLoggingInterceptor loggerInterceptorHeader = new HttpLoggingInterceptor();
        loggerInterceptorHeader.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient client = (new OkHttpClient.Builder()).addInterceptor(loggerInterceptorBody).addInterceptor(loggerInterceptorHeader).readTimeout(30L, TimeUnit.SECONDS).connectTimeout(30L, TimeUnit.SECONDS).build();
        Retrofit retrofit = (new Retrofit.Builder()).baseUrl(Constants.IMDBBaseAddress).client(client).addConverterFactory(GsonConverterFactory.create()).build();

        return retrofit.create(serviceClass);
    }
}
